<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property int $id [int]
 * @property int $product_id [int]
 * @property string $product_image [varchar(255)]
 */
class StoreProduct extends ActiveRecord
{

    public static function tableName(): string
    {
        return '{{%store_product}}';
    }
}