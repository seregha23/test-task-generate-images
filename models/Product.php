<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 *
 * @property int $id [int]
 * @property string $image [varchar(255)]
 * @property bool $is_deleted [tinyint(1)]
 *
 * @property-read null|StoreProduct $storeProducts
 */
class Product extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%product}}';
    }


    public function getStoreProducts(): ActiveQuery
    {
        return $this->hasOne(StoreProduct::class, ['product_id' => 'id']);
    }
}