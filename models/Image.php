<?php

namespace app\models;

use Yii;
use Intervention\Image\ImageManagerStatic;
use Exception;

class Image
{

    /** @throws Exception */
    public static function generateMiniature(string $url, array $sizes): string
    {
        $name       = self::getNameFromUrl($url);
        $pathToSave = '/web/images/' . $name;

        ImageManagerStatic::make($url)->resize($sizes['width'], $sizes['height'])->save($pathToSave);

        if (!file_exists($pathToSave)) {
            throw new Exception('Not generated with watermark');
        }

        return self::getUrlFromImageStorage($pathToSave);

    }

    /** @throws Exception */
    public static function generateWatermarkedMiniature(string $url, array $sizes): string
    {
        $name       = self::getNameFromUrl($url);
        $pathToSave = '/web/images/' . $name;

        if (isset(Yii::$app->params['watermark']['name'])) {
            $image = ImageManagerStatic::make($url)->resize($sizes[0], $sizes[1]);

            $image->insert(
                '/web/images/watermarks/' . Yii::$app->params['watermark']['name'],
                Yii::$app->params['watermark']['position'],
                Yii::$app->params['watermark']['positionX'],
                Yii::$app->params['watermark']['positionY']
            );

            $image->save($pathToSave);
        }

        if (!file_exists($pathToSave)) {
            throw new Exception('Not generated with watermark');
        }

        return self::getUrlFromImageStorage($pathToSave);
    }

    public static function getNameFromUrl(string $url): string
    {
        return substr($url,strrpos($url,"/") + 1);
    }

    public static function getUrlFromImageStorage(string $path): string
    {
        return Yii::$app->params['siteUrl'] . '/static/images/' . pathinfo( $path , PATHINFO_BASENAME );
    }
}
