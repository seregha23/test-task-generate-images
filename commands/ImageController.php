<?php

namespace app\commands;

use app\services\ImagesGenerateService;
use yii\console\Controller;

class ImageController extends Controller
{

    /** php -f yii image/generating-thumbnails 100 true false
     * @throws \Exception
     */
    public function actionGeneratingThumbnails(string $sizes, bool $watermarked = false, bool $catalogOnly = true)
    {
        $imagesGenerateService = new ImagesGenerateService($sizes, $watermarked, $catalogOnly);

        $imagesGenerateService->generatedImages();
    }
}
