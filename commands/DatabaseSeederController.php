<?php

namespace app\commands;

use tebazil\yii2seeder\Seeder;
use yii\console\Controller;

class DatabaseSeederController extends Controller
{

    /** php -f yii database-seeder/generating */
    public function actionGenerating($message = 'hello world')
    {
        $seeder    = new Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker     = $generator->getFakerConfigurator();

        $seeder->table('product')->columns([
            'id', //automatic pk
            'is_deleted' => $faker->boolean(0)
        ])->rowQuantity(5);

        $seeder->table('store_product')->columns([
            'id', //automatic pk
            'product_id'    => $generator->relation('product', 'id'),
            'product_image' => $faker->imageUrl()
        ])->rowQuantity(5);

        $seeder->refill();
    }
}
