<?php

namespace app\services;

use app\models\Image;
use app\models\Product;
use Exception;

class ImagesGenerateService
{
    public string $url = '';

    public int $width  = 0;

    public int $height = 0;

    public bool $watermarked = false;

    public bool $catalogOnly = false;

    public function __construct(string $sizes, bool $watermarked,  bool $catalogOnly)
    {

        $this->watermarked = $watermarked;
        $this->catalogOnly = $catalogOnly;

        $this->prepareSizes($sizes);
    }

    /** @throws Exception */
    public function generate(string $url): string {
        if ($this->watermarked) {
            return Image::generateWatermarkedMiniature($url, [$this->width, $this->height]);
        }
        return Image::generateMiniature($url, [$this->width, $this->height]);
    }

    public function prepareSizes(string $sizes): void
    {
        $width  = $sizes;
        $height = $sizes;

        if (str_contains('x', $sizes)) {
            [$width, $height] = explode('x', $sizes);
        }

        $this->width  = $width;
        $this->height = $height;
    }

    public function generatedImages(): array
    {
        $generatedImage    = 0;
        $notGeneratedImage = 0;

        $products = Product::find()->joinWith('')->all();
        foreach ($products as $product) {
            foreach ($product->storeProducts as $storeProduct) {
                try {

                    $this->generate($storeProduct->product_image);
                    $generatedImage++;

                } catch (\Exception $e) {
                    $notGeneratedImage++;
                }
            }
        }

        return [
            'generatedImage'    => $generatedImage,
            'notGeneratedImage' => $notGeneratedImage,
        ];
    }
}