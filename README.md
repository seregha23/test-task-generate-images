
## Запуск проекта
    Создать БД 
        CREATE DATABASE test_task CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

    Выполнить след команды:
    - cd existing_repo
    - git remote add origin https://gitlab.com/seregha23/test-task-generate-images.git
    - git pull

    Далее там же из проекта выполнить:     
    - sudo composer install --ignore-platform-reqs
    Далее выполнить
    - php yii migrate - мигарции
    - php -f yii database-seeder/generating   - заполнение таблицы фейковыми данными

