<?php

return [
    'siteUrl'   => 'test.loc',
    'watermark' => [
        'name'      => 'watermark.png',
        'position'  => 'center',
        'positionX' => 0,
        'positionY' => 0,
    ],
];
